# Sample Size Planning for Nonparametric Non-Inferiority Analyses in the Gold Standard Design

## Description
This repository contains the data and R-scripts used for the analysis conducted in the thesis on Sample Size Planning for Nonparametric Non-Inferiority Analyses in the Gold Standard Design. The analysis involved simulation studies on continuous data to explore possible strategies for sample size planning when analysing with the studentized permutation test by M�tze et. al. (2017) and the nonparametric test based on classical midranks by Munzel (2009).

## Software Requirements
The analysis was performed using R (version 4.2.3). Additional R packages required include ThreeArmedTrials (version 1.0.5), rankFD (version 0.1.1), dplyr (version 1.1.1), doParallel (version 1.0.17), foreach (version 1.5.2). 

## Structure
The repository is organised into two folders, each corresponding to one of the considered tests. Each folder contains the R-scripts used for the simulations as well as the data obtained from the simulations. Additionally, the repository includes the implementation of the Munzel test, which is planned to be adopted in the package ThreeArmedTrials by M�tze (2023).

## Simulation R-scripts
The first part of the file names indicates the objective of the simulation script, while the second part indicates whether the simulation was conducted under the null or alternative hypothesis to investigate the type I error rate or power behaviour, respectively. Additional information is provided in the third part of the file name. The following abbrevations are used: perm = studentized permutation test; munzel = munzel test; ssr = sample size re-estimation; OSU = Blinded adjusted one-sample variance estimator; UG = Unblinded group-variance estimator. 

## Further R-scripts
For the studentized permutation test, the repository includes the custom code for the test statistic, which was used after the discovery of the coding error in the ThreeArmedTrials package. The code is provided alongside the simulation scripts.
Regarding the Munzel test, the repository includes the implementation in R, which can be found in the R-folder. Additionally, there is a separate folder called "package code" that contains the implementation of the Munzel test intended for adoption in the ThreeArmedTrials package. This folder also provides a description of the functionalities of the code.

## Data
The results of the simulation studies are listed in the corresponding data files of each test. Both raw and processed data are provided. Files that include "power" or "typeIerror" indicate the processed data of the simulation results. The abbreviations as mentioned above apply.

## Authors and acknowledgment
This thesis was submitted on June 1st, 2023 by Maxi Schulz at the University of G�ttingen (Department of Medical Statistics). The author would like to thank Prof. Dr. Tim Friede, Dr. Tobias M�tze, Dr. Thomas Asendorf and Prof. Dr. Frank Konietschke for the assistance throughout this thesis.

## Citation
If you use the data or scripts in this repository, please cite: Schulz, Maxi. "Sample Size Planning for Nonparametric Non-Inferiority Analyses in the Gold Standard Design." University of G�ttingen, 2023.

## References
Munzel, U. (2009). Nonparametric non-inferiority analyses in the three-arm design with active control and placebo. Statistics in Medicine, 28, 3643�3656. https://doi.org/10.1002/sim.3727

M�tze, T. (2023). ThreeArmedTrials: Design and analysis of clinical non-inferiority or superiority trials with active and placebo control [R package version 1.0-5]. https: //CRAN.R-project.org/package=ThreeArmedTrials

M�tze, T., Konietschke, F., Munk, A., & Friede, T. (2017). A studentized permutation test for three-arm trials in the �gold standard� design. Statistics in Medicine, 36, 883�898. https://doi.org/10.1002/sim.7176



